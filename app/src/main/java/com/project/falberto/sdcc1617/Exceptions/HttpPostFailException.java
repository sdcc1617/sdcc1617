package com.project.falberto.sdcc1617.Exceptions;

/**
 * Created by falberto on 19/04/17.
 */

public class HttpPostFailException extends Exception {

    public HttpPostFailException(String message) {
        super(message);
    }
}

