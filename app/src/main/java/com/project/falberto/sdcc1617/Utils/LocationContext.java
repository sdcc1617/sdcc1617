package com.project.falberto.sdcc1617.Utils;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by falberto on 16/08/17.
 */

public class LocationContext {
    private static final LocationContext getInstance = new LocationContext();

    private String longitude;
    private String latitude;
    private LatLng farLeft;
    private LatLng farRight;
    private LatLng nearLeft;
    private LatLng nearRight;
    private GoogleMap map;

    public GoogleMap getMap() {
        return map;
    }

    public static LocationContext getInstance() {
        return getInstance;
    }

    private LocationContext() {
    }

    public void setMap(GoogleMap map) {
        this.map = map;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public LatLng getFarLeft() {
        return farLeft;
    }

    public void setFarLeft(LatLng farLeft) {
        this.farLeft = farLeft;
    }

    public LatLng getFarRight() {
        return farRight;
    }

    public void setFarRight(LatLng farRight) {
        this.farRight = farRight;
    }

    public LatLng getNearLeft() {
        return nearLeft;
    }

    public void setNearLeft(LatLng nearLeft) {
        this.nearLeft = nearLeft;
    }

    public LatLng getNearRight() {
        return nearRight;
    }

    public void setNearRight(LatLng nearRight) {
        this.nearRight = nearRight;
    }
}
