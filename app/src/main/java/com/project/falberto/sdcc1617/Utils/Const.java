package com.project.falberto.sdcc1617.Utils;


public class Const {

    /**
     * Possibili valori assunti dalla luminosità del display
     */
    static public final int BrightnessMin=80;
    static public final int BrightnessMedium=150;
    static public final int BrightnessMax=200;

    /**
     * Possibili valori assunti dal tempo di polling
     */
    static public final int PollingTimeSlow=60000;
    static public final int PollingTimeMedium=30000;
    static public final int PollingTimeDefault= 5000;

    /**
     * Possibili valori assunti dal tempo di monitoring
     */
    static public final int MonitoringTimeSlow=300000;
    static public final int MonitoringTimeMedium=200000;
    static public final int MonitoringTimeDefault=120000;

    //set load balancer address
    public static String serverAddress = //"http://sdcc1617-loadbalancerGoService-1755470414.eu-west-1.elb.amazonaws.com:8082";// "http://sdcc1617-loadbalancerGoService-202312581.eu-west-1.elb.amazonaws.com:8082";//"http://sdcc1617-loadbalancerGoService-330098181.eu-west-1.elb.amazonaws.com:80";
    "http://sdcc1617-loadbalancerGoService-1290511819.eu-west-1.elb.amazonaws.com:8082";
}
