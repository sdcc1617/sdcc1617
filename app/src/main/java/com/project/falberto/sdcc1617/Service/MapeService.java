package com.project.falberto.sdcc1617.Service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.project.falberto.sdcc1617.MapeAdaptiveCycle.Analysis;
import com.project.falberto.sdcc1617.MapeAdaptiveCycle.Execute;
import com.project.falberto.sdcc1617.MapeAdaptiveCycle.Monitoring;
import com.project.falberto.sdcc1617.MapeAdaptiveCycle.Plan;
import com.project.falberto.sdcc1617.MapeAdaptiveCycle.Planning;
import com.project.falberto.sdcc1617.MapeAdaptiveCycle.State;
import com.project.falberto.sdcc1617.Utils.Const;
import com.project.falberto.sdcc1617.View.Main.MainActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

/**
 * Service che si occupa di effettuare il ciclo MAPE per
 * l'autoadattamento per il risparmio energetico
 */

public class MapeService extends Service {

    private static int CHECK_BATTERY_INTERVAL = 5000;
    private static int monitoringTime ;
    private Monitoring monitoring;
    private int batteryLevel;// = 46,count = 0;
    private int percentOld = -1;
    private State oldState =  null;
    private Handler handler;
    SharedPreferences.OnSharedPreferenceChangeListener spChanged;
    Boolean adaptive = true;

    private SharedPreferences pref;

    private Runnable mapeRunnable = new Runnable() {
        @Override
        public void run() {

            try {
                batteryLevel = monitoring.getBatteryAgent().getLevelBattery();
                /*count++;
                if(count%5 ==0){
                    batteryLevel++;
                }*/
                State newState = Analysis.analyze(batteryLevel);

                Log.i("Service", batteryLevel + "");

                if (oldState == null)
                    oldState = newState;
                Plan plan = Planning.plan(newState, oldState);
                Log.i("OLDSTATEn", oldState.name());
                Log.i("NEWSTATEn", newState.name());
                Execute.executePlan(plan);
                oldState = newState;
            }catch (Exception e){
                stopSelf();
                return;
            }
            handler.postDelayed(mapeRunnable,  monitoringTime );
        }
    };

    @Override
    public void onCreate() {

        if(adaptive) {
            handler = new Handler();

            pref = getSharedPreferences("MyPreferences", MainActivity.MODE_PRIVATE);
            monitoringTime = pref.getInt("MonitorTime",
                    Const.MonitoringTimeDefault);
            handler.postDelayed(mapeRunnable, CHECK_BATTERY_INTERVAL);
            monitoring = Monitoring.getInstance();
            spChanged = new
                SharedPreferences.OnSharedPreferenceChangeListener() {
                    @Override
                    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                        Log.i("monitoring", "richiamo");
                        if (key.equals("MonitorTime")) {
                            CHECK_BATTERY_INTERVAL = sharedPreferences.getInt("MonitorTime",
                                    Const.MonitoringTimeDefault);
                            Log.i("monitoring", "valore identificato: " + CHECK_BATTERY_INTERVAL);
                        }
                    }
                };
            pref = getSharedPreferences("MyPreferences", MainActivity.MODE_PRIVATE);
            pref.registerOnSharedPreferenceChangeListener(spChanged);
        }
        this.registerReceiver(this.BatteryInfo, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

    }

    @Override
    public void onDestroy() {
        handler.removeCallbacks(mapeRunnable);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }


    private BroadcastReceiver BatteryInfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int scale = intent.getIntExtra(BatteryManager.EXTRA_SCALE, 100);
            boolean isPresent = intent.getBooleanExtra("present", false);
            //int status= intent.getIntExtra(BatteryManager.EXTRA_STATUS,0);
            //int  health= intent.getIntExtra(BatteryManager.EXTRA_HEALTH,0);
            //int  plugged= intent.getIntExtra(BatteryManager.EXTRA_PLUGGED,0);
            //int  voltage= intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE,0);
            Bundle bundle = intent.getExtras();
            String str = bundle.toString();
            StringBuilder sb = new StringBuilder();
            if (isPresent) {
                int percent = (level * 100) / scale;
                BatteryManager mBatteryManager = (BatteryManager) getSystemService(Context.BATTERY_SERVICE);
                Long avgCurrent = null, currentNow = null;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    avgCurrent = mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_AVERAGE);
                    currentNow = mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);
                    //sb.append("Technology: " + bundle.getString("technology") + "\n");
                    //sb.append("Voltage: " + voltage + "mV" + "\n");
                    //sb.append("Temperature: " + bundle.getInt("temperature") + "\n");
                    sb.append("Current: " + currentNow + "mAh" + "\n");
                    //sb.append("Health: " + getHealthString(health) + "\n");
                    //sb.append("Charging: " + getStatusString(status) +"\n");
                    //sb.append(getPlugTypeString(plugged) + "\n");
                    sb.append("" + percent + "%\n");
                    // String timeStamp = Long.toString(Calendar.getInstance().getTime().getTime());
                    //sb.append("" + timeStamp + "\n");
                    long t = (long) System.currentTimeMillis()/1000;
                }
                else{
                    sb.append("No Lollipop, Marshmallow o Nougat!!!");
                    // currentNow = mBatteryManager.getLongProperty(BatteryManager.BATTERY_PROPERTY_CURRENT_NOW);

                    //sb.append("Technology: " + bundle.getString("technology") + "\n");
                    //sb.append("Voltage: " + voltage + "mV" + "\n");
                    //sb.append("Temperature: " + bundle.getInt("temperature") + "\n");
                    // sb.append("Current: " + currentNow + "mAh" + "\n");
                    //sb.append("Health: " + getHealthString(health) + "\n");
                    //sb.append("Charging: " + getStatusString(status) +"\n");
                    //sb.append(getPlugTypeString(plugged) + "\n");
                    sb.append("" + percent + "%\n");
                    //String timeStamp = Long.toString(Calendar.getInstance().getTime().getTime());
                    //sb.append("" + timeStamp + "\n");
                    long t = (long) System.currentTimeMillis()/1000;
                }
            } else {
                sb.append("Battery not present!!!");
            }
            Log.i("Battery", sb.toString());
            //Log.i("Battery", Integer.toString(status));

        }
    };

    private String getPlugTypeString(int plugged) {
        String plugType = "Unknown";

        switch (plugged) {
            case BatteryManager.BATTERY_PLUGGED_AC:
                plugType = "AC";
                break;
            case BatteryManager.BATTERY_PLUGGED_USB:
                plugType = "USB";
                break;
        }
        return plugType;
    }

    private String getHealthString(int health) {
        String healthString = "Unknown";
        switch (health) {
            case BatteryManager.BATTERY_HEALTH_DEAD:
                healthString = "Dead";
                break;
            case BatteryManager.BATTERY_HEALTH_GOOD:
                healthString = "Good Condition";
                break;
            case BatteryManager.BATTERY_HEALTH_OVER_VOLTAGE:
                healthString = "Over Voltage";
                break;
            case BatteryManager.BATTERY_HEALTH_OVERHEAT:
                healthString = "Over Heat";
                break;
            case BatteryManager.BATTERY_HEALTH_UNSPECIFIED_FAILURE:
                healthString = "Failure";
                break;
        }
        return healthString;
    }
    private String getStatusString(int status) {
        String statusString = "Unknown";

        switch (status) {
            case BatteryManager.BATTERY_STATUS_CHARGING:
                statusString = "Charging";
                break;
            case BatteryManager.BATTERY_STATUS_DISCHARGING:
                statusString = "Discharging";
                break;
            case BatteryManager.BATTERY_STATUS_FULL:
                statusString = "Full";
                break;
            case BatteryManager.BATTERY_STATUS_NOT_CHARGING:
                statusString = "Not Charging";
                break;
        }
        return statusString;
    }
}

