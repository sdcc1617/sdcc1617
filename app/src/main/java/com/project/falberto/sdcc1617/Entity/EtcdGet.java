package com.project.falberto.sdcc1617.Entity;

/**
 * Created by falberto on 13/12/17.
 */

public class EtcdGet {
        private String action;
        private NodeEtcd node;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public NodeEtcd getNode() {
        return node;
    }

    public void setNode(NodeEtcd node) {
        this.node = node;
    }
}


