package com.project.falberto.sdcc1617.Entity;

import org.json.JSONObject;

/**
 * Created by falberto on 13/12/17.
 */

public class NodeEtcd {
    private  String key;
    private Boolean dir;
    private String nodes;
    private int modifiedIndex;
    private  int createdIndex;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Boolean getDir() {
        return dir;
    }

    public void setDir(Boolean dir) {
        this.dir = dir;
    }

    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public int getModifiedIndex() {
        return modifiedIndex;
    }

    public void setModifiedIndex(int modifiedIndex) {
        this.modifiedIndex = modifiedIndex;
    }

    public int getCreatedIndex() {
        return createdIndex;
    }

    public void setCreatedIndex(int createdIndex) {
        this.createdIndex = createdIndex;
    }

    public NodeEtcd(String key, Boolean dir, String nodes, int modifiedIndex, int createdIndex) {
        this.key = key;
        this.dir = dir;
        this.nodes = nodes;
        this.modifiedIndex = modifiedIndex;
        this.createdIndex = createdIndex;
    }

    public NodeEtcd(JSONObject jsonObject) throws org.json.JSONException {

        this.key = jsonObject.getString("key");
        this.dir = jsonObject.getBoolean("dir");
        this.nodes = jsonObject.getString("nodes");

        this.modifiedIndex = jsonObject.getInt("modifiedIndex");
        this.createdIndex = jsonObject.getInt("createdIndex");

    }
}

