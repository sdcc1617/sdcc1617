package com.project.falberto.sdcc1617.MapeAdaptiveCycle;

import android.util.Log;

/**
 * Created by falberto on 08/03/17.
 * Componente di analisi del ciclo MAPE
 */

public class Analysis {

    private static final int LOW = 40;
    private static final int MEDIUM = 60;


    public static State analyze(int levelBattery){
        State state;

        if(levelBattery<LOW){
            state = State.LOWPOWER;
            Log.i("ANALYSIS", "LOWPOWER");
        }
        else  if(levelBattery < MEDIUM){
            state = State.POWERSAVING;
            Log.i("ANALYSIS", "POWERSAVING");
        }
        else{
            state = State.FULLPOWER;
            Log.i("ANALYSIS", "FULLPOWER");
        }

        return  state;
    }
}
