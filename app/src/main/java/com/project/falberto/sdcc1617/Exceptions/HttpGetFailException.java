package com.project.falberto.sdcc1617.Exceptions;

/**
 * Created by falberto on 30/04/17.
 */

public class HttpGetFailException extends Exception {

    public HttpGetFailException(String message) {
        super(message);
    }
}