package com.project.falberto.sdcc1617.View.Camera;

/**
 * Created by falberto on 27/03/17.
 */

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.project.falberto.sdcc1617.Utils.HTTPUtils;
import com.project.falberto.sdcc1617.R;
import com.project.falberto.sdcc1617.Utils.MyDisplay;
import com.project.falberto.sdcc1617.View.Main.MainActivity;

public class CameraActivity extends AppCompatActivity {
    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageView;
    private EditText titleEditText;
    private EditText messageEditText;
    private Spinner spinner;

    private Bitmap photo;
    private String longitude;
    private String latitude;
    private String by;
    private String topic;
    private String title;
    private String message;
    private Window window;
    private SharedPreferences.OnSharedPreferenceChangeListener spChanged;
    private SharedPreferences pref;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mycamera);
        toolbarGuiCreate();
        buttonGuiCreate();
        spinnerGuiCreate();
        editTextGuiInitialize();

        pref = getSharedPreferences("MyPreferences", MainActivity.MODE_PRIVATE);

        //get longitude e latitude from MainActivity
        Intent intent = getIntent();
        longitude = intent.getStringExtra("LONGITUDE");
        latitude = intent.getStringExtra("LATITUDE");
        by = intent.getStringExtra("BY");
        /**
         * Inizializzo il livello di luminosità con quello esterno all'applicazione, rispetto al
         * livello a cui attualmente dovrebbe essere in funzione del livello della batteria
         */
        window = getWindow();
        if (MyDisplay.checkMaxBrightness(this, pref))
            MyDisplay.setBrightness(pref, window);

        /**
         * Intercetto i cambiamenti dei valori nello shared preferences per la luminosità
         */
        spChanged = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals("Brightness")) {
                    MyDisplay.setBrightness(sharedPreferences, window);
                }

            }
        };
        pref.registerOnSharedPreferenceChangeListener(spChanged);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK) {
            photo = (Bitmap) data.getExtras().get("data");
            imageView.setImageBitmap(photo);
        }
    }

    private void toolbarGuiCreate(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbarMyCamera);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

    }

    private void buttonGuiCreate(){
        this.imageView = (ImageView)this.findViewById(R.id.imageView1);
        Button photoButton = (Button) this.findViewById(R.id.button1);
        photoButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });
    }

    public void editTextGuiInitialize(){
        this.titleEditText = (EditText) findViewById(R.id.editText3);
        this.messageEditText = (EditText) findViewById(R.id.editText);
    }


    private void spinnerGuiCreate(){
        spinner = (Spinner) findViewById(R.id.spinner2);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.signalation_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        View v = getCurrentFocus();

        if (v != null &&
                (ev.getAction() == MotionEvent.ACTION_UP || ev.getAction() == MotionEvent.ACTION_MOVE) &&
                v instanceof EditText &&
                !v.getClass().getName().startsWith("android.webkit.")) {
            int scrcoords[] = new int[2];
            v.getLocationOnScreen(scrcoords);
            float x = ev.getRawX() + v.getLeft() - scrcoords[0];
            float y = ev.getRawY() + v.getTop() - scrcoords[1];

            if (x < v.getLeft() || x > v.getRight() || y < v.getTop() || y > v.getBottom())
                hideKeyboard(this);
        }
        return super.dispatchTouchEvent(ev);
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mycamera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_send) {
            SendSignalationAsyncTask sendAsyncTask = new SendSignalationAsyncTask();
            sendAsyncTask.execute();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private void sendSignalation(){

        title = titleEditText.getText().toString();
        message = messageEditText.getText().toString();
        topic = spinner.getSelectedItem().toString();

        HTTPUtils.postSignalation(longitude, latitude, topic, title, message, photo,by);

    }

    /**
     * Asyncktask relativa alla post della segnalazione al servizio scritto in Go
     */
    class SendSignalationAsyncTask extends AsyncTask<String, Void, Boolean> {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected Boolean doInBackground(String... urls) {
            sendSignalation();
            return false;
        }
        @Override
        protected void onPostExecute(Boolean bool) {
            View messageView = getLayoutInflater().inflate(R.layout.toast_signalation, null, false);
            TextView textView = (TextView) messageView.findViewById(R.id.textViewToast);
            int defaultColor = textView.getTextColors().getDefaultColor();
            textView.setTextColor(defaultColor);
            AlertDialog.Builder builder = new AlertDialog.Builder(CameraActivity.this);
            builder.setMessage("Grazie")
                    //s.setTitle("Error")
                    .setIcon(R.mipmap.ic_launcher)
                    .setView(messageView)
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            onBackPressed();
                        }
                    }).create().show();
        }
    }

    public void showToast() {
        View messageView = getLayoutInflater().inflate(R.layout.toast_signalation, null, false);

        // When linking text, force to always use default color. This works
        // around a pressed color state bug.
        TextView textView = (TextView) messageView.findViewById(R.id.textViewToast);
        int defaultColor = textView.getTextColors().getDefaultColor();
        textView.setTextColor(defaultColor);
        AlertDialog.Builder builder = new AlertDialog.Builder(CameraActivity.this);
        builder.setIcon(R.mipmap.ic_launcher);
        builder.setTitle(R.string.app_name);
       ;
        builder.create();
        builder.show();
    }
}



