package com.project.falberto.sdcc1617.Utils;
import android.content.Context;

public class MainActivityContext {
    private static final MainActivityContext ourInstance = new MainActivityContext();

    private Context context ;
    public static MainActivityContext getInstance() {
        return ourInstance;
    }

    private MainActivityContext() {
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context){
        this.context = context;
    }
}
