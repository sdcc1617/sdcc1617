package com.project.falberto.sdcc1617.Entity;

import org.json.JSONObject;

/**
 * Created by falberto on 13/12/17.
 */

public class EtcdGetOneValue {
    private String action;
    private NodeEtcdOneValue node;


    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public NodeEtcdOneValue getNode() {
        return node;
    }

    public void setNode(NodeEtcdOneValue node) {
        this.node = node;
    }

}
