package com.project.falberto.sdcc1617.Utils;

import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.project.falberto.sdcc1617.Entity.Signalation;
import com.project.falberto.sdcc1617.R;
import com.project.falberto.sdcc1617.View.Main.MainActivity;

import java.util.LinkedList;

/**
 * Created by falberto on 17/08/17.
 */

public class MapUtils {

    public static BitmapDescriptor getMarkerColor(String topic){
        MainActivityContext mainActivityContext = MainActivityContext.getInstance();
        if(topic.equals(mainActivityContext.getContext().getResources().getString(R.string.spinner_object1)))
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
        else if(topic.equals(mainActivityContext.getContext().getResources().getString(R.string.spinner_object2)))
            return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);

        else return BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN);

    }

    /**
     * Metodo che si occupa del rendering della mappa. Prima cancello quello che è presente, dopo di che ricostruisco la vista
     * @param map
     * @param signalationLinkedList
     */
    public static void Rendering(GoogleMap map, LinkedList<Signalation> signalationLinkedList){
        map.clear();
        for(Signalation s:signalationLinkedList){


           Marker m = map.addMarker(new MarkerOptions()
                    .position(new LatLng(Double.parseDouble(s.getLatitude()), Double.parseDouble(s.getLongitude())))
                    .title("Title: "+ s.getTitle())
                    .icon(getMarkerColor(s.getTopic()))
                    .snippet("Comment: "+ s.getMessage() +"\n"+
                             "By: "+ s.getBy() +"\n"+
                             "Owner: "+s.getOwner())
                    );
            //m.showInfoWindow();
        }

    }
}
