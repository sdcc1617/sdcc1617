package com.project.falberto.sdcc1617.Entity;

import android.graphics.Bitmap;

import org.json.JSONObject;

/**
 * Created by falberto on 30/04/17.
 *
 * Entity relativa alla segnalazione
 */

public class Signalation {

    private String topic;
    private String title;
    private String message;
    private String longitude;
    private String latitude;
    private String url;
    private String due;
    private String stateId;
    private String owner;
    private String by;

    private String state;
    private Bitmap bmImg = null;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDue() {
        return due;
    }

    public void setDue(String due) {
        this.due = due;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Bitmap getBmImg() {
        return bmImg;
    }

    public void setBmImg(Bitmap bmImg) {
        this.bmImg = bmImg;
    }

    public Signalation(JSONObject jsonObject) throws org.json.JSONException {

        this.topic = jsonObject.getString("topic");
        this.title = jsonObject.getString("title");
        this.message = jsonObject.getString("message");
        ;
        this.longitude = jsonObject.getString("longitude");
        this.latitude = jsonObject.getString("latitude");
        this.url = jsonObject.getString("url");
        this.due = jsonObject.getString("due");
        this.stateId = jsonObject.getString("stateId");
        this.owner = jsonObject.getString("owner");
        this.by = jsonObject.getString("by");
    }
    /*
    @Override
    public String toString() {
        return "Signalation{" +
                "topic='" + topic + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", url='" + url + '\'' +
                ", due='" + due + '\'' +
                '}';
    }*/

    @Override
    public String toString() {
        return "Signalation{" +
                "topic='" + topic + '\'' +
                ", title='" + title + '\'' +
                ", message='" + message + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", url='" + url + '\'' +
                ", due='" + due + '\'' +
                ", stateId='" + stateId + '\'' +
                ", owner='" + owner + '\'' +
                ", by='" + by + '\'' +
                ", state='" + state + '\'' +
                ", bmImg=" + bmImg +
                '}';
    }
}
