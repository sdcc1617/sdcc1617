package com.project.falberto.sdcc1617.Service;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.project.falberto.sdcc1617.Entity.Signalation;
import com.project.falberto.sdcc1617.R;
import com.project.falberto.sdcc1617.Utils.Const;
import com.project.falberto.sdcc1617.Utils.LocationContext;
import com.project.falberto.sdcc1617.Utils.MainActivityContext;
import com.project.falberto.sdcc1617.Utils.MapUtils;
import com.project.falberto.sdcc1617.View.Main.MainActivity;

import java.util.LinkedList;

import static com.project.falberto.sdcc1617.Utils.HTTPUtils.getSignalations;

/**
 * Service che ha il compito di eseguire periodicamente la chiamata REST per aggiornare la mappa
 */

public class RestPollingService extends Service {
    static String longitude;
    static String latitude;
    static int PollingTime;
    NotificationManager mNotifyMgr;
    NotificationCompat.Builder mBuilder;
    private LinkedList<Signalation> signalationLinkedList;
    private GoogleMap mMap;
    private Runnable restPollingRunnable = new Runnable() {
        @Override
        public void run() {
            Log.e("RestPollingService", "INIZIO");

            while (true) {

                LocationContext locationContext = LocationContext.getInstance();
                if(locationContext.getFarLeft() !=null &&
                        locationContext.getFarRight() !=null &&
                    locationContext.getNearLeft()!=null &&
                        locationContext.getNearRight() !=null & locationContext.getMap()!=null) {
                    LatLng farLeft = locationContext.getFarLeft();
                    LatLng farRight = locationContext.getFarRight();
                    LatLng nearLeft = locationContext.getNearLeft();
                    LatLng nearRight = locationContext.getNearRight();
                    mMap = locationContext.getMap();
                    //Qui metto la richiesta http e aggiorno la mappa
                    //doRequest()
                    //updateMap()
                    //Log.i("MAIN", Double.toString(mLastKnownLocation.getLatitude()));
                    //Log.i("MAIN", Double.toString(mLastKnownLocation.getLongitude()));

                    final Double farLeftLong = farLeft.longitude;
                    final Double farLeftLat = farLeft.latitude;
                    final Double nearLeftLong = nearLeft.longitude;
                    final Double nearLeftLat = nearLeft.latitude;
                    final Double farRightLong = farRight.longitude;
                    final Double farRightLat = farRight.latitude;
                    final Double nearRightLong = nearRight.longitude;
                    final Double nearRightLat = nearRight.latitude;
                    Log.i("RestPollingService", "farLeftLong: " + farLeftLong + " " + "farLeftLat: " + farLeftLat);
                    Log.i("RestPollingService", "farRightLong: " + farRightLong + " " + "farRightLat: " + farRightLat);
                    Log.i("RestPollingService", "nearLeftLong: " + nearLeftLong + " " + "nearLeftLat: " + nearLeftLat);
                    Log.i("RestPollingService", "nearRightLong: " + nearRightLong + " " + "nearRightLat: " + nearRightLat);

                    signalationLinkedList = getSignalations(farLeftLong, farRightLong,
                            nearLeftLat, farRightLat);
                    Log.i("MAIN", "longSx: " + farLeftLong + " " + "longDx: " + farRightLong + " " +
                            "latSx: " + nearLeftLat + " " + "latDx: " + farRightLat);

                    new Handler().post(new Runnable() {
                        @Override
                        public void run() {
                            // Code here will run in UI thread

                            if (signalationLinkedList != null) {
                                //updateMap
                                for (Signalation s : signalationLinkedList) {
                                    Log.i("GETSIGNALATION", s.toString());
                                }
                                MapUtils.Rendering(mMap, signalationLinkedList);
                            }
                        }
                    });
                }
                Log.i("RestPollingService","RestPollingService inizia a dormire: "+PollingTime);
                if(LocationContext.getInstance().getLongitude()!=null)
                    Log.i("RestPollingService", LocationContext.getInstance().getLongitude());
                if (LocationContext.getInstance().getLatitude()!=null)
                    Log.i("RestPollingService", LocationContext.getInstance().getLatitude());
                try {
                        Thread.sleep(PollingTime);
                } catch (InterruptedException e) {
                    Log.i("RestPollingService","Ops RestPollingService si è svegliato");
                    e.printStackTrace();
                }


                Log.i("RestPollingService","RestPollingService si sveglia");
            }
        }
    };


    @Override
    public void onCreate() {

        SharedPreferences pref = getSharedPreferences("MyPreferences", MainActivity.MODE_PRIVATE);

        PollingTime = pref.getInt("PollingTime", Const.PollingTimeDefault);

        //Quando faccio una get ricevo una notifica che mi avvisa
        mBuilder = new NotificationCompat.Builder(MainActivityContext.getInstance().getContext())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Hai una nuova notifica")
                .setContentText("Hai una nuova notifica");

        mNotifyMgr =  (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        /**
         * Mi registro sui cambiamenti di PollingTime nelle shared preferences
         */
        SharedPreferences.OnSharedPreferenceChangeListener spChanged = new
                SharedPreferences.OnSharedPreferenceChangeListener() {
                    @Override
                    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                        if(key.equals("PollingTime"))
                            PollingTime = sharedPreferences.getInt("PollingTime", Const.PollingTimeDefault);
                    }
                };

        pref.registerOnSharedPreferenceChangeListener(spChanged);

        if(LocationContext.getInstance().getLongitude()!=null)
            Log.i("RestPollingService", LocationContext.getInstance().getLongitude());
        if (LocationContext.getInstance().getLatitude()!=null)
            Log.i("RestPollingService", LocationContext.getInstance().getLatitude());

        Log.i("RestPollingService","Creazione service RestPollingService");
        //Avvio il thread dal service
        (new Thread(restPollingRunnable)).start();
    }

    @Override
    public void onDestroy() {}

    @Override
    public IBinder onBind(Intent intent) { return null; }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) { return START_STICKY;  }

}