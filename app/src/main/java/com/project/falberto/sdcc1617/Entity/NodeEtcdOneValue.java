package com.project.falberto.sdcc1617.Entity;

import org.json.JSONObject;

/**
 * Created by falberto on 13/12/17.
 */

public class NodeEtcdOneValue {
        private String key;
        private String value;
        private int modifiedIndex;
        private int createdIndex;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getModifiedIndex() {
        return modifiedIndex;
    }

    public void setModifiedIndex(int modifiedIndex) {
        this.modifiedIndex = modifiedIndex;
    }

    public int getCreatedIndex() {
        return createdIndex;
    }

    public void setCreatedIndex(int createdIndex) {
        this.createdIndex = createdIndex;
    }

    public NodeEtcdOneValue(JSONObject jsonObject) throws org.json.JSONException {
        this.key = jsonObject.getString("key");
        this.value = jsonObject.getString("value");
        this.modifiedIndex = jsonObject.getInt("modifiedIndex");
        this.createdIndex = jsonObject.getInt("createdIndex");

    }
}

