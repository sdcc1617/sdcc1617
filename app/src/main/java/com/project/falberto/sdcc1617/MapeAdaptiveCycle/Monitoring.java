package com.project.falberto.sdcc1617.MapeAdaptiveCycle;

/**
 * Created by falberto on 08/03/17.
 * Blocco di Monitoring del ciclo MAPE
 */

public class Monitoring {

    private BatteryAgent batteryAgent;
    private int levelBattery;

    private static Monitoring mInstance = null;

    private Monitoring(){

        batteryAgent = BatteryAgent.getInstance();
    }


    public static Monitoring getInstance(){
        if(mInstance == null)
        {
            mInstance = new Monitoring();
        }
        return mInstance;
    }

    public BatteryAgent getBatteryAgent(){
        return batteryAgent;
    }
}
