package com.project.falberto.sdcc1617.MapeAdaptiveCycle;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;

/**
 * Created by falberto on 08/03/17.
 * Agente singleton relativo alla batteria
 */

public class BatteryAgent {

    private static BatteryAgent mInstance = null;

    private int levelBattery;
    private BroadcastReceiver mReceiver;

    private BatteryAgent(){

        mReceiver = new BatteryBroadcastReceiver();
    }


    public static BatteryAgent getInstance(){
        if(mInstance == null)
        {
            mInstance = new BatteryAgent();
        }
        return mInstance;
    }

    public void registerBroadcastReceiver(Context context, IntentFilter filter){
        context.registerReceiver(mReceiver, filter);
    }


    public void unregisterBroadcastReceiver(Context context){
        context.unregisterReceiver(mReceiver);
    }
    private class BatteryBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            levelBattery = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            //mBatteryLevelText.setText(getString(R.string.battery_level) + " " + level);
            //ProgressBar mBatteryLevelProgress = (ProgressBar) findViewById(R.id.progressBar);
            //mBatteryLevelProgress.setProgress(levelBattery);

        }
    }

    public int getLevelBattery(){
        return levelBattery;
    }
}
