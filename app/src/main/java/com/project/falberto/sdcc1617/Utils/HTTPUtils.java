package com.project.falberto.sdcc1617.Utils;

import android.graphics.Bitmap;
import android.util.Log;

import com.project.falberto.sdcc1617.Entity.Signalation;
import com.project.falberto.sdcc1617.Exceptions.HttpGetFailException;
import com.project.falberto.sdcc1617.Exceptions.HttpPostFailException;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedList;


/**
 * Created by falberto on 18/04/17.
 */

public class HTTPUtils {

    public static void postSignalation(String longitude, String latitude, String topic, String title,
                                 String message, Bitmap photo, String by){
        String attachmentName = "photo";
        String crlf = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";

        try {
            URL url = new URL(Const.serverAddress + "/receiveUserSignalation");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            //conn.setReadTimeout(10000);
            //conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);

            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            Bitmap bitmap = photo;

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            byte[] byteArray;
            if(photo!=null){
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);

            }
            byteArray = stream.toByteArray();

            DataOutputStream request = new DataOutputStream(conn.getOutputStream());
            request.writeBytes(twoHyphens + boundary + crlf);
            request.writeBytes("Content-Disposition: form-data; name=\"" + attachmentName + "\"" + crlf);
            request.writeBytes("Content-Type: image/jpeg" + crlf);
            request.writeBytes(crlf);
            if(photo!=null) {
                request.write(byteArray);
            }
            else{
                byteArray = new byte[4];
                byteArray[0] = 0;
                byteArray[1] = 0;
                byteArray[2] = 1;
                byteArray[3] = 1;
                request.write(byteArray);
            }
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + crlf);

            request.writeBytes("Content-Disposition: form-data; name=\"topic\"" + crlf);
            request.writeBytes(crlf);
            request.writeBytes(topic);
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + crlf);

            request.writeBytes("Content-Disposition: form-data; name=\"title\"" + crlf);
            request.writeBytes(crlf);
            request.writeBytes(title);
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + crlf);

            request.writeBytes("Content-Disposition: form-data; name=\"message\"" + crlf);
            request.writeBytes(crlf);
            request.writeBytes(message);
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + crlf);

            request.writeBytes("Content-Disposition: form-data; name=\"longitude\"" + crlf);
            request.writeBytes(crlf);
            request.writeBytes(longitude);
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + crlf);

            request.writeBytes("Content-Disposition: form-data; name=\"latitude\"" + crlf);
            request.writeBytes(crlf);
            request.writeBytes(latitude);
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + crlf);

            request.writeBytes("Content-Disposition: form-data; name=\"by\"" + crlf);
            request.writeBytes(crlf);
            request.writeBytes(by);
            request.writeBytes(crlf);
            request.writeBytes(twoHyphens + boundary + twoHyphens);

            request.flush();
            request.close();

            int statusCode = conn.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_OK) {
                InputStream in  = new BufferedInputStream(conn.getInputStream());
                String response = org.apache.commons.io.IOUtils.toString(in, "UTF-8");
                //JSONObject jsonObject = new JSONObject(response);
            }
            else
            {
                Log.e("code", Integer.toString(statusCode));
                throw new HttpPostFailException("Fail to do Post! Try again later!");
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static LinkedList<Signalation>
            getSignalations(double longsx, double longdx, double latsx, double latdx){
        LinkedList<Signalation> listSignalations = new LinkedList<Signalation> ();
        try {
            URL url = new URL(Const.serverAddress + "/getItemsFromSignalationsCollection/"+
                    Double.toString(longsx)+"/"+
                    Double.toString(longdx) +"/"+
                    Double.toString(latsx)+"/"+
                    Double.toString(latdx));
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-length", "0");
            conn.setUseCaches(false);
            conn.setAllowUserInteraction(false);
            conn.setConnectTimeout(100000);
            conn.setReadTimeout(100000);
            conn.connect();

            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                listSignalations = new LinkedList<>();
                // create JSON object from content
                InputStream in = new BufferedInputStream( conn.getInputStream());

                JSONArray jsonArray = new JSONArray(IOUtils.toString(in,"UTF-8"));
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject explrObject = jsonArray.getJSONObject(i);
                    Signalation signalation = new Signalation(explrObject);
                    listSignalations.add(signalation);
                }

            }
            else{
                throw new HttpGetFailException("Fail to do Get! Try again later!");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return listSignalations;
    }
}
