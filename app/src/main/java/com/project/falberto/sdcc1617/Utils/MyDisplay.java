package com.project.falberto.sdcc1617.Utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;


/**
 * Created by diego on 21/03/17.
 */

public class MyDisplay {

    public static void setBrightness(SharedPreferences preference, Window window){

        int brightness = preference.getInt("Brightness", Const.BrightnessMax);

        WindowManager.LayoutParams layoutpars = window.getAttributes();

        layoutpars.screenBrightness = brightness  / (float)255; //brightness  / (float)255;

        window.setAttributes(layoutpars);
    }

    public static boolean checkMaxBrightness(Context context, SharedPreferences preference){
        int brightness = preference.getInt("Brightness", Const.BrightnessMax);

        ContentResolver cResolver;
        cResolver = context.getContentResolver();
        int systemBrightness = 255;
        try{
            systemBrightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS);
        }
        catch (Settings.SettingNotFoundException e){
            e.printStackTrace();
        }
        if(systemBrightness < brightness)
            return false;

        return true;
    }
}
