package com.project.falberto.sdcc1617.MapeAdaptiveCycle;

import android.content.SharedPreferences;
import android.provider.Settings;
import android.util.Log;

import com.project.falberto.sdcc1617.Utils.Const;
import com.project.falberto.sdcc1617.Utils.MainActivityContext;
import com.project.falberto.sdcc1617.View.Main.MainActivity;

/**
 * Created by falberto on 08/03/17.
 * Blocco di Execute del ciclo MAPE
 */

public class Execute {

    public static void executePlan(Plan plan){

        if (plan == Plan.LOW2POWERSAVING){
            low2PowerSaving();
            Log.i("EXECUTE", "Plan.LOW2POWERSAVING");
        }
        else if(plan == Plan.FULL2POWESAVING){
            full2PowerSaving();
            Log.i("EXECUTE", "Plan.FULL2POWESAVING");
        }
        else if(plan == Plan.POWERSAVING2FULL){
            powerSaving2Full();
            Log.i("EXECUTE", "Plan.POWERSAVING2FULL");
        }
        else if(plan == Plan.POWERSAVING2LOW){
            powerSaving2Low();
            Log.i("EXECUTE", "Plan.POWERSAVING2LOW");
        }
         else if(plan == Plan.DEFAULT){
            //doNothing();
            Log.i("EXECUTE", "Plan.DEFAULT");
        }
    }

    public static void low2PowerSaving() {

        SharedPreferences pref = MainActivityContext.getInstance().getContext()
                .getSharedPreferences("MyPreferences", MainActivity.MODE_PRIVATE);

        SharedPreferences.Editor editor = pref.edit();

        int brightnessLevel = 0;

        try {
            brightnessLevel = Settings.System.getInt(MainActivityContext.getInstance()
                    .getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
        }

        if (brightnessLevel > Const.BrightnessMedium //&& pref.getString("displayONOFF", "true").equals("true")
             ){
            editor.putInt("Brightness", Const.BrightnessMedium);
            editor.commit();
        }
        //if(pref.getString("PollingONOFF", "true").equals("true")) {
            //sleep
            editor.putInt("PollingTime", Const.PollingTimeMedium);
            editor.commit();
        //}
            //MonitorTime
        editor.putInt("MonitorTime", Const.MonitoringTimeMedium);
        editor.commit();

    }
    public static void full2PowerSaving(){

        SharedPreferences pref = MainActivityContext.getInstance().getContext()
                .getSharedPreferences("MyPreferences",MainActivity.MODE_PRIVATE);

        SharedPreferences.Editor editor =  pref.edit();

        int brightnessLevel = 0;

        try {
            brightnessLevel = Settings.System.getInt(MainActivityContext.getInstance().getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {}

        if(brightnessLevel>Const.BrightnessMedium )// && pref.getString("displayONOFF", "true").equals("true"))
            {
                editor.putInt("Brightness", Const.BrightnessMedium);
                editor.commit();
            }


        //if(pref.getString("PollingONOFF", "true").equals("true")) {
            //sleep
            editor.putInt("PollingTime", Const.PollingTimeMedium);
            editor.commit();
       // }
        //MonitorTime
        editor.putInt("MonitorTime",Const.MonitoringTimeMedium);
        editor.commit();

        //batch server
        //RabbitMQSender.getInstance().sendMessageThread(message +pref.getString("username","")+"\",\"value\":\""+ Const.BufferDefault +"\"}");

        //invio quantità per fare batch

    }
    public static void powerSaving2Full(){

        SharedPreferences pref = MainActivityContext.getInstance().getContext().getSharedPreferences("MyPreferences", MainActivity.MODE_PRIVATE);

        SharedPreferences.Editor editor =  pref.edit();

        int brightnessLevel = 0;

        try {
            brightnessLevel = Settings.System.getInt(MainActivityContext.getInstance().getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {}

        if(brightnessLevel>Const.BrightnessMax )//&& pref.getString("displayONOFF", "true").equals("true"))
            editor.putInt("Brightness", Const.BrightnessMax);

       // if(pref.getString("PollingONOFF", "true").equals("true"))
            //sleep
            editor.putInt("PollingTime",Const.PollingTimeDefault);

        //MonitorTime
        editor.putInt("MonitorTime",Const.MonitoringTimeDefault);

        editor.commit();


    }
    public static void powerSaving2Low(){

        SharedPreferences pref = MainActivityContext.getInstance().getContext()
                .getSharedPreferences("MyPreferences",MainActivity.MODE_PRIVATE);

        SharedPreferences.Editor editor =  pref.edit();

        int brightnessLevel = 0;

        try {
            brightnessLevel = Settings.System.getInt(MainActivityContext.getInstance().getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {}

        if(brightnessLevel>Const.BrightnessMin)// && pref.getString("displayONOFF", "true").equals("true"))
            editor.putInt("Brightness", Const.BrightnessMin);

       // if(pref.getString("PollingONOFF", "true").equals("true"))
            //sleep
            editor.putInt("PollingTime",Const.PollingTimeSlow);

        //MonitorTime
        editor.putInt("MonitorTime",Const.MonitoringTimeSlow);

        editor.commit();

    }

    public static void doNothing(){
  /*
        SharedPreferences pref = MainActivityContext.getInstance().getContext().getSharedPreferences("MyPreferences",MainActivity.MODE_PRIVATE);

        SharedPreferences.Editor editor =  pref.edit();

        int brightnessLevel = 0;

        try {
            brightnessLevel = Settings.System.getInt(MainActivityContext.getInstance().getContext().getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {}

        if(brightnessLevel>Const.BrightnessMedium && pref.getString("displayONOFF", "true").equals("true"))
            editor.putInt("Brightness", Const.BrightnessMedium);

        if(pref.getString("PollingONOFF", "true").equals("true"))
            //sleep
                editor.putInt("PollingTime",Const.PollingDefault);

        //MonitorTime
        editor.putInt("MonitorTime",Const.MonitoringTimeDefault);

        //batch server
        //invio quantità per fare batch

        editor.commit();
    */
    }

}
