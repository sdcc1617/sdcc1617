package com.project.falberto.sdcc1617.View.Main;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoDevice;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUser;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserAttributes;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserCodeDeliveryDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserDetails;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.CognitoUserSession;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GenericHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.GetDetailsHandler;
import com.amazonaws.mobileconnectors.cognitoidentityprovider.handlers.UpdateAttributesHandler;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.project.falberto.sdcc1617.Entity.EtcdGetOneValue;
import com.project.falberto.sdcc1617.Entity.NodeEtcd;
import com.project.falberto.sdcc1617.Entity.NodeEtcdOneValue;
import com.project.falberto.sdcc1617.Entity.Signalation;
import com.project.falberto.sdcc1617.Exceptions.HttpGetFailException;
import com.project.falberto.sdcc1617.MapeAdaptiveCycle.BatteryAgent;

import com.project.falberto.sdcc1617.R;
import com.project.falberto.sdcc1617.Service.MapeService;
import com.project.falberto.sdcc1617.Service.RestPollingService;
import com.project.falberto.sdcc1617.Utils.Cognito.AppHelper;
import com.project.falberto.sdcc1617.Utils.Const;
import com.project.falberto.sdcc1617.Utils.HTTPUtils;
import com.project.falberto.sdcc1617.Utils.LocationContext;
import com.project.falberto.sdcc1617.Utils.MainActivityContext;
import com.project.falberto.sdcc1617.Utils.MapUtils;
import com.project.falberto.sdcc1617.Utils.MyDisplay;
import com.project.falberto.sdcc1617.View.Camera.CameraActivity;
import com.project.falberto.sdcc1617.View.Cognito.AddAttributeActivity;
import com.project.falberto.sdcc1617.View.Cognito.ChangePasswordActivity;
import com.project.falberto.sdcc1617.View.Cognito.DeviceSettingsActivity;
import com.project.falberto.sdcc1617.View.Cognito.SettingsActivity;
import com.project.falberto.sdcc1617.View.Cognito.UserActivity;
import com.project.falberto.sdcc1617.View.Cognito.UserAttributesAdapter;
import com.project.falberto.sdcc1617.View.Cognito.VerifyActivity;
import com.project.falberto.sdcc1617.View.Intro.IntroActivity;

import org.apache.commons.io.IOUtils;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Classe principale dell'applicazione. La MainActivity è quella che contiene la mappa
 */

public class MainActivity extends AppCompatActivity implements
        //NavigationView.OnNavigationItemSelectedListener,
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION =1;
    private Location mLastKnownLocation;
    private CameraPosition mCameraPosition;
    private static final int DEFAULT_ZOOM = 18;
    private float zoom = DEFAULT_ZOOM;
    private double moveX = 0.0;
    private double moveY = 0.0;
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";


    private static final String TAG = MainActivity.class.getSimpleName();

    private FloatingActionButton fab;
    private Toolbar toolbar;

    private SharedPreferences.OnSharedPreferenceChangeListener spChanged;
    private SharedPreferences pref;

    private BatteryAgent batteryAgent;
    private Window window;

    private LinkedList<Signalation> signalationLinkedList;



    private NavigationView nDrawer;
    private DrawerLayout mDrawer;

    private ActionBarDrawerToggle mDrawerToggle;

    private AlertDialog userDialog;
    private ProgressDialog waitDialog;
    private ListView attributesList;

    // Cognito user objects
    private CognitoUser user;
    private CognitoUserSession session;
    private CognitoUserDetails details;

    // User details
    private String username;

    // To track changes to user details
    private final List<String> attributesToDelete = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mLastKnownLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        setContentView(R.layout.activity_main);

        toolbarGuiCreate();
        setFloatingActionButton();
        //setDrawlerLayout();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        mGoogleApiClient.connect();

        startIntro();
        initApplicationServices();

        // Set navigation drawer for this screen
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawer, toolbar, R.string.nav_drawer_open, R.string.nav_drawer_close);
        mDrawer.addDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        nDrawer = (NavigationView) findViewById(R.id.nav_view);
        setNavDrawer();
        init();
        View navigationHeader = nDrawer.getHeaderView(0);
        TextView navHeaderSubTitle = (TextView) navigationHeader.findViewById(R.id.textViewNavUserSub);
        navHeaderSubTitle.setText(username);
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mLastKnownLocation);
            super.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    /**
     * Handles failure to connect to the Google Play services client.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // Refer to the reference doc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }

    /**
     * Handles suspension of the connection to the Google Play services client.
     */
    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Play services connection suspended");
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Do other setup activities here too, as described elsewhere in this tutorial.

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();
        // Get the current location of the device and set the position of the map.
        getDeviceLocation();

        if(mLastKnownLocation!=null){
            LocationContext.getInstance().setLongitude(Double.toString(mLastKnownLocation.getLongitude()));
            LocationContext.getInstance().setLatitude(Double.toString(mLastKnownLocation.getLatitude()));
            LocationContext.getInstance().setFarLeft(mMap.getProjection().getVisibleRegion().farLeft);
            LocationContext.getInstance().setFarRight( mMap.getProjection().getVisibleRegion().farRight);
            LocationContext.getInstance().setNearLeft( mMap.getProjection().getVisibleRegion().nearLeft);
            LocationContext.getInstance().setNearRight(mMap.getProjection().getVisibleRegion().nearRight);
            LocationContext.getInstance().setMap(mMap);
        }
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener(){
            @Override
            public void onCameraIdle(){
             //   pollingTask();
            }
        });
        /**
         *  Se viene modificato lo zoom viene fatta una get delle segnalazioni richiedendole al servizio scritto in Go,
         *  relativo al rettangolo visibile sullo screen del device
         *
         */
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener(){
            @Override
            public void onCameraChange(CameraPosition position){

                if(zoom != position.zoom) {
                    zoom = position.zoom;
                    Log.i("ZOOM", Float.toString(zoom));
                    //nuova richiesta

                    Log.i("MAIN", Double.toString(mLastKnownLocation.getLatitude()));
                    Log.i("MAIN", Double.toString(mLastKnownLocation.getLongitude()));
                    LocationContext.getInstance().setLongitude(Double.toString(mLastKnownLocation.getLongitude()));
                    LocationContext.getInstance().setLatitude(Double.toString(mLastKnownLocation.getLatitude()));
                    LocationContext.getInstance().setFarLeft(mMap.getProjection().getVisibleRegion().farLeft);
                    LocationContext.getInstance().setFarRight( mMap.getProjection().getVisibleRegion().farRight);
                    LocationContext.getInstance().setNearLeft( mMap.getProjection().getVisibleRegion().nearLeft);
                    LocationContext.getInstance().setNearRight(mMap.getProjection().getVisibleRegion().nearRight);
                    LocationContext.getInstance().setMap(mMap);
                    LatLng farLeft = mMap.getProjection().getVisibleRegion().farLeft;
                    LatLng farRight = mMap.getProjection().getVisibleRegion().farRight;
                    LatLng nearLeft = mMap.getProjection().getVisibleRegion().nearLeft;
                    LatLng nearRight = mMap.getProjection().getVisibleRegion().nearRight;
                    Double farLeftLong = farLeft.longitude;
                    Double farLeftLat = farLeft.latitude;
                    Double nearLeftLong = nearLeft.longitude;
                    Double nearLeftLat = nearLeft.latitude;
                    Double farRightLong = farRight.longitude;
                    Double farRightLat = farRight.latitude;
                    Double nearRightLong = nearRight.longitude;
                    Double nearRightLat = nearRight.latitude;
                    Log.i("MAIN", "farLeftLong: "+ farLeftLong + " " + "farLeftLat: "+ farLeftLat);
                    Log.i("MAIN", "farRightLong: "+ farRightLong + " " + "farRightLat: "+ farRightLat);
                    Log.i("MAIN", "nearLeftLong: "+ nearLeftLong + " " + "nearLeftLat: "+ nearLeftLat);
                    Log.i("MAIN", "nearRightLong: "+ nearRightLong + " " + "nearRightLat: "+ nearRightLat);

                    Log.i("MAIN", "farLeftLong: " + farLeftLong + " "+
                            "farRightLong: " + farRightLong + " " +
                            "farLeftLat: " + nearLeftLat + " " +
                            "farRightLat: " + farRightLat);

                    GetSignalationAsyncTask getAsyncTask = new GetSignalationAsyncTask(farLeftLong, farRightLong,
                            nearLeftLat, farRightLat);

                    getAsyncTask.execute();

                }
                /**
                 *  Se viene spostata la mappa di una quantità considerevole, viene fatta una get delle segnalazioni richiedendole
                 *  al servizio scritto in Go, relativo al rettangolo visibile sullo screen del device
                 *
                 */
                else{

                    Log.i("CAMERA_MOVE", "Camera_Move");

                    Log.i("MAIN", Double.toString(mLastKnownLocation.getLatitude()));
                    Log.i("MAIN", Double.toString(mLastKnownLocation.getLongitude()));
                    LatLng farLeft = mMap.getProjection().getVisibleRegion().farLeft;
                    LatLng farRight = mMap.getProjection().getVisibleRegion().farRight;
                    LatLng nearLeft = mMap.getProjection().getVisibleRegion().nearLeft;
                    LatLng nearRight = mMap.getProjection().getVisibleRegion().nearRight;
                    LocationContext.getInstance().setLongitude(Double.toString(mLastKnownLocation.getLongitude()));
                    LocationContext.getInstance().setLatitude(Double.toString(mLastKnownLocation.getLatitude()));
                    LocationContext.getInstance().setFarLeft(mMap.getProjection().getVisibleRegion().farLeft);
                    LocationContext.getInstance().setFarRight( mMap.getProjection().getVisibleRegion().farRight);
                    LocationContext.getInstance().setNearLeft( mMap.getProjection().getVisibleRegion().nearLeft);
                    LocationContext.getInstance().setNearRight(mMap.getProjection().getVisibleRegion().nearRight);
                    LocationContext.getInstance().setMap(mMap);
                    Double farLeftLong = farLeft.longitude;
                    Double farLeftLat = farLeft.latitude;
                    Double nearLeftLong = nearLeft.longitude;
                    Double nearLeftLat = nearLeft.latitude;
                    Double farRightLong = farRight.longitude;
                    Double farRightLat = farRight.latitude;
                    Double nearRightLong = nearRight.longitude;
                    Double nearRightLat = nearRight.latitude;
                    Log.i("MAIN", "farLeftLong: "+ farLeftLong + " " + "farLeftLat: "+ farLeftLat);
                    Log.i("MAIN", "farRightLong: "+ farRightLong + " " + "farRightLat: "+ farRightLat);
                    Log.i("MAIN", "nearLeftLong: "+ nearLeftLong + " " + "nearLeftLat: "+ nearLeftLat);
                    Log.i("MAIN", "nearRightLong: "+ nearRightLong + " " + "nearRightLat: "+ nearRightLat);

                    Log.i("MAIN", "farLeftLong: " + farLeftLong + " "+
                                "farRightLong: " + farRightLong + " " +
                            "farLeftLat: " + nearLeftLat + " " +
                            "farRightLat: " + farRightLat);

                    if((farRightLong - moveX) *(farRightLong - moveX) +
                            (farRightLat - moveY) *(farRightLat - moveY) > 0.1 ){
                        GetSignalationAsyncTask getAsyncTask = new GetSignalationAsyncTask(farLeftLong, farRightLong,
                                nearLeftLat, farRightLat);

                        getAsyncTask.execute();
                    }
                    moveX = farRightLong;
                    moveY = farRightLat;

                }
            }
        });

        // Setting a custom info window adapter for the google map
        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {

                // Getting view from the layout file info_window_layout
                View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                Bitmap bitmap = null;
                String state = null;
                // Getting the position from the marker
                LatLng latLng = arg0.getPosition();
                String title = arg0.getTitle();
                String message = arg0.getSnippet();
                for(Signalation s:signalationLinkedList){
                    if(latLng.equals(new LatLng(Double.parseDouble(s.getLatitude()),Double.parseDouble(s.getLongitude())))){
                        if(s.getBmImg()!=null)
                            bitmap = s.getBmImg();
                        if(s.getState()!=null) {
                            state = s.getState();
                        }
                        else state = "IN_ASSIGNMENT";
                    }
                }
                // Getting reference to the TextView to set latitude
                TextView tvLat = (TextView) v.findViewById(R.id.info_window_title);

                // Getting reference to the TextView to set longitude
                TextView tvLng = (TextView) v.findViewById(R.id.info_window_message);

                TextView tvState= (TextView) v.findViewById(R.id.info_window_state);

                // Setting the latitude
                tvLat.setText(title);


                // Setting the longitude
                tvLng.setText(message);
                tvState.setText("State: " + state);
                if(bitmap!=null) {
                    ImageView imageView = (ImageView) v.findViewById(R.id.info_window_imageView);
                    imageView.setImageBitmap(bitmap);
                }
                // Returning the view containing InfoWindow contents
                return v;


            }
        });

    }
    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    /**
     *  Viene fatta una get delle segnalazioni richiedendole al servizio scritto in Go, relativo al rettangolo visibile sullo screen del device,
     *  la prima volta che viene riaperta l'applicazione
     *
     */
    private void getDeviceLocation() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        if (mLocationPermissionGranted) {
            mLastKnownLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        }

        // Set the map's camera position to the current location of the device.
        if (mCameraPosition != null) {
            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
        } else if (mLastKnownLocation != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(mLastKnownLocation.getLatitude(),
                            mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
        } else {
            Log.d(TAG, "Current location is null. Using defaults.");
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
        }

        // faccio una chiamata per popolare la mappa

        if(mLastKnownLocation!=null) {
            LocationContext.getInstance().setLongitude(Double.toString(mLastKnownLocation.getLongitude()));
            LocationContext.getInstance().setLatitude(Double.toString(mLastKnownLocation.getLatitude()));
            LocationContext.getInstance().setFarLeft(mMap.getProjection().getVisibleRegion().farLeft);
            LocationContext.getInstance().setFarRight( mMap.getProjection().getVisibleRegion().farRight);
            LocationContext.getInstance().setNearLeft( mMap.getProjection().getVisibleRegion().nearLeft);
            LocationContext.getInstance().setNearRight(mMap.getProjection().getVisibleRegion().nearRight);
            LocationContext.getInstance().setMap(mMap);
            Log.i("MAIN", Double.toString(mLastKnownLocation.getLatitude()));
            Log.i("MAIN", Double.toString(mLastKnownLocation.getLongitude()));
            LatLng farLeft = mMap.getProjection().getVisibleRegion().farLeft;
            LatLng farRight = mMap.getProjection().getVisibleRegion().farRight;
            LatLng nearLeft = mMap.getProjection().getVisibleRegion().nearLeft;
            LatLng nearRight = mMap.getProjection().getVisibleRegion().nearRight;
            Double farLeftLong = farLeft.longitude;
            Double farLeftLat = farLeft.latitude;
            Double nearLeftLong = nearLeft.longitude;
            Double nearLeftLat = nearLeft.latitude;
            Double farRightLong = farRight.longitude;
            Double farRightLat = farRight.latitude;
            Double nearRightLong = nearRight.longitude;
            Double nearRightLat = nearRight.latitude;
            Log.i("MAIN", "farLeftLong: "+ farLeftLong + " " + "farLeftLat: "+ farLeftLat);
            Log.i("MAIN", "farRightLong: "+ farRightLong + " " + "farRightLat: "+ farRightLat);
            Log.i("MAIN", "nearLeftLong: "+ nearLeftLong + " " + "nearLeftLat: "+ nearLeftLat);
            Log.i("MAIN", "nearRightLong: "+ nearRightLong + " " + "nearRightLat: "+ nearRightLat);

            moveX = farRightLong;
            moveY = farRightLat;
            GetSignalationAsyncTask getAsyncTask = new GetSignalationAsyncTask(farLeftLong, farRightLong,
                    nearLeftLat, farRightLat);

            getAsyncTask.execute();
        }
        /*
        if (pref.getBoolean("Intro", true) && mLastKnownLocation!=null ) {
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("LONGITUDE", Double.toString(mLastKnownLocation.getLongitude()));
            editor.putString("LATITUDE", Double.toString(mLastKnownLocation.getLatitude()));
            editor.commit();
        }*/
    }

    /**
     *  Metodo che si occupa di fare delle request http get agli eventuali URL relativi a Cloudfront,
     *  e presenti all'interno del body restituito dalla get al servizio in Go e relativo alle segnalazioni
     *
     */

    private LinkedList<Signalation> getSignalations(Double longSx, Double longDx,
                                                    Double latSx, Double latDx){

        LinkedList<Signalation> signalationLinkedList = HTTPUtils.getSignalations(longSx, longDx, latSx, latDx);

        for(Signalation s: signalationLinkedList){
            if(s.getUrl()!=null) {
                try {
                    Log.i("URL", s.getUrl());
                    URL url = new URL(s.getUrl());
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setDoInput(true);
                    conn.connect();
                    InputStream is = conn.getInputStream();
                    Bitmap bmImg = BitmapFactory.decodeStream(is);

                    bmImg = Bitmap.createScaledBitmap(bmImg, 128, 128, true);
                    s.setBmImg(bmImg);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if(s.getStateId() !=null && !s.getOwner().equals("")) {
                try {
                    Log.i("URL", s.getStateId());
                    Log.e("URL", Const.serverAddress +"/getEtcdObjectFromKey/"+s.getOwner() + "/" +s.getStateId());
                    URL url = new URL(Const.serverAddress +"/getEtcdObjectFromKey/"+s.getOwner() + "/" +s.getStateId());
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setRequestProperty("Content-length", "0");
                    conn.setUseCaches(false);
                    conn.setAllowUserInteraction(false);
                    conn.setConnectTimeout(100000);
                    conn.setReadTimeout(100000);
                    conn.connect();
                    int responseCode = conn.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        InputStream in = new BufferedInputStream( conn.getInputStream());
                        JSONObject explrObject =  new JSONObject(IOUtils.toString(in,"UTF-8"));
                        //EtcdGetOneValue etcdGetOneValue = new EtcdGetOneValue(explrObject);
                        JSONObject nodeObject = explrObject.getJSONObject("node");
                        NodeEtcdOneValue nodeEtcdOneValue = new NodeEtcdOneValue(nodeObject);
                        String[] ss= nodeEtcdOneValue.getValue().split("\\|");
                        s.setState( ss[0]);
                        Log.e("JSON", s.getState());
                    }
                    else{
                        throw new HttpGetFailException("Fail to do Get! Try again later!");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        //se devo leggere lo stato delle segnalazioni devo fare un altro ciclo qui, che fa la richiesta
        //devo mettere a signalation un campo state
        //questa cosa posso farla dopo.. ora non serve
        this.signalationLinkedList = signalationLinkedList;

        return signalationLinkedList;
    }

    /**
     * Asyncktask relativa alla get delle segnalazioni
     */
    class GetSignalationAsyncTask extends AsyncTask<Double, Void, LinkedList<Signalation>> {

        private Double longSx;
        private Double longDx;
        private Double latSx;
        private Double latDx;
        public GetSignalationAsyncTask(Double longSx, Double longDx,
                                       Double latSx, Double latDx) {
            super();
            this.longSx = longSx;
            this.longDx = longDx;
            this.latSx = latSx;
            this.latDx =latDx;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected LinkedList<Signalation>  doInBackground(Double... urls) {
            LinkedList<Signalation> signalationLinkedList = getSignalations(longSx, longDx, latSx,
                    latDx);
            Log.i("MAIN", "longSx: " + longSx + " " + "longDx: " + longDx + " " +
                            "latSx: " + latSx + " " + "latDx: " + latDx);

            return signalationLinkedList;
        }
        @Override
        protected void onPostExecute(LinkedList<Signalation> signalationLinkedList) {
            if(signalationLinkedList!= null){
                //updateMap
                for(Signalation s: signalationLinkedList){
                    Log.i("GETSIGNALATION", s.toString());
                }
                MapUtils.Rendering(mMap, signalationLinkedList);
            }

        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // 2
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // 3
        if( mGoogleApiClient != null && mGoogleApiClient.isConnected() ) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onBackPressed() {
        /*
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }*/
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
*/
    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }

        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }

        if (mLocationPermissionGranted) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mLastKnownLocation = null;
        }
    }


    private void toolbarGuiCreate(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

    }
    private void setFloatingActionButton() {

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CameraActivity.class);
                if (mLastKnownLocation != null) {
                    intent.putExtra("LATITUDE", Double.toString(mLastKnownLocation.getLatitude()));
                    intent.putExtra("LONGITUDE", Double.toString(mLastKnownLocation.getLongitude()));
                    intent.putExtra("BY", username);

                    startActivity(intent);
                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "Problem with position! Try later";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            }
        });
    }

   /* private void setDrawlerLayout() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
*/
    /**
     * Salvo anche i valori iniziali nella SharedPreferences la prima volta per l'adattamento
     */

    private void startIntro(){

        pref = getSharedPreferences("MyPreferences", MainActivity.MODE_PRIVATE);
        if (!pref.getBoolean("Intro", false)) {
            //Intent intent = new Intent(this, com.project.falberto.sdcc1617.View.Cognito.MainActivity.class);
            //startActivity(intent);
            SharedPreferences.Editor editor = pref.edit();
            editor.putInt("Brightness", Const.BrightnessMax);
            editor.putInt("MonitorTime", Const.MonitoringTimeDefault);
            editor.putInt("PollingTime", Const.PollingTimeDefault);
            editor.putBoolean("Intro", true);
            editor.commit();
        }
        Map<String, ?> allEntries = pref.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            Log.d("map values", entry.getKey() + ": " + entry.getValue().toString());
        }
    }

    private void initApplicationServices(){


        /**
         * Salvo il MainContext
         */
        MainActivityContext.getInstance().setContext(getApplicationContext());

        /**
         * Inizio il singleton del batteryAgent
         */
        batteryAgent = BatteryAgent.getInstance();

        /**
         * Registro il broadcast receiver per il monitoring del livello della batteria
         */
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        batteryAgent.registerBroadcastReceiver(getApplicationContext(), filter);

        /**
         * Inizializzo il livello di luminosità con quello esterno all'applicazione, rispetto al
         * livello a cui attualmente dovrebbe essere in funzione del livello della batteria
         */
        window = getWindow();
        if (MyDisplay.checkMaxBrightness(this, pref))
            MyDisplay.setBrightness(pref, window);

        /**
         * Intercetto i cambiamenti dei valori nello shared preferences per la luminosità
         */
        spChanged = new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equals("Brightness")) {
                    MyDisplay.setBrightness(sharedPreferences, window);
                }

            }
        };
        pref.registerOnSharedPreferenceChangeListener(spChanged);

        LocationContext.getInstance();
        /**
         * Avvio il service del ciclo Mape
         */
        Intent mapeCycle = new Intent(this, MapeService.class);
        startService(mapeCycle);
        /**
         * Avvio il service del restPolling
         */
        Intent restPolling = new Intent(this, RestPollingService.class);
        startService(restPolling);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_user_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Find which menu item was selected
        int menuItem = item.getItemId();

        // Do the task
        if(menuItem == R.id.user_update_attribute) {
            //updateAllAttributes();
            showWaitDialog("Updating...");
            getDetails();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 20:
                // Settings
                if(resultCode == RESULT_OK) {
                    boolean refresh = data.getBooleanExtra("refresh", true);
                    if (refresh) {
                        //showAttributes();
                    }
                }
                break;
            case 21:
                // Verify attributes
                if(resultCode == RESULT_OK) {
                    boolean refresh = data.getBooleanExtra("refresh", true);
                    if (refresh) {
                        //showAttributes();
                    }
                }
                break;
            case 22:
                // Add attributes
                if(resultCode == RESULT_OK) {
                    boolean refresh = data.getBooleanExtra("refresh", true);
                    if (refresh) {
                        //showAttributes();
                    }
                }
                break;
        }
    }

    // Handle when the a navigation item is selected
    private void setNavDrawer() {
        nDrawer.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                performAction(item);
                return true;
            }
        });
    }

    // Perform the action for the selected navigation item
    private void performAction(MenuItem item) {
        // Close the navigation drawer
        mDrawer.closeDrawers();

        // Find which item was selected
        switch(item.getItemId()) {
            case R.id.nav_user_add_attribute:
                // Add a new attribute
                addAttribute();
                break;

            case R.id.nav_user_change_password:
                // Change password
                changePassword();
                break;
            case R.id.nav_user_verify_attribute:
                // Confirm new user
                // confirmUser();
                attributesVerification();
                break;
            case R.id.nav_user_settings:
                // Show user settings
                showSettings();
                break;
            case R.id.nav_user_sign_out:
                // Sign out from this account
                signOut();
                break;
            case R.id.nav_user_trusted_devices:
                showTrustedDevices();
                break;
        }
    }

    // Get user details from CIP service
    private void getDetails() {
        AppHelper.getPool().getUser(username).getDetailsInBackground(detailsHandler);
    }
/*
    // Show user attributes from CIP service
    private void showAttributes() {
        final UserAttributesAdapter attributesAdapter = new UserAttributesAdapter(getApplicationContext());
        final ListView attributesListView;
        attributesListView = (ListView) findViewById(R.id.listViewUserAttributes);
        attributesListView.setAdapter(attributesAdapter);
        attributesList = attributesListView;

        attributesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView data = (TextView) view.findViewById(R.id.editTextUserDetailInput);
                String attributeType = data.getHint().toString();
                String attributeValue = data.getText().toString();
                showUserDetail(attributeType, attributeValue);
            }
        });
    }*/

    // Update attributes
    private void updateAttribute(String attributeType, String attributeValue) {

        if(attributeType == null || attributeType.length() < 1) {
            return;
        }
        CognitoUserAttributes updatedUserAttributes = new CognitoUserAttributes();
        updatedUserAttributes.addAttribute(attributeType, attributeValue);
        Toast.makeText(getApplicationContext(), attributeType + ": " + attributeValue, Toast.LENGTH_LONG);
        showWaitDialog("Updating...");
        AppHelper.getPool().getUser(AppHelper.getCurrUser()).updateAttributesInBackground(updatedUserAttributes, updateHandler);
    }

    // Show user MFA Settings
    private void showSettings() {
        Intent userSettingsActivity = new Intent(this,SettingsActivity.class);
        startActivityForResult(userSettingsActivity, 20);
    }

    // Add a new attribute
    private void addAttribute() {
        Intent addAttrbutesActivity = new Intent(this,AddAttributeActivity.class);
        startActivityForResult(addAttrbutesActivity, 22);
    }

    // Delete attribute
    private void deleteAttribute(String attributeName) {
        showWaitDialog("Deleting...");
        List<String> attributesToDelete = new ArrayList<>();
        attributesToDelete.add(attributeName);
        AppHelper.getPool().getUser(AppHelper.getCurrUser()).deleteAttributesInBackground(attributesToDelete, deleteHandler);
    }

    // Change user password
    private void changePassword() {
        Intent changePssActivity = new Intent(this, ChangePasswordActivity.class);
        startActivity(changePssActivity);
    }

    // Verify attributes
    private void attributesVerification() {
        Intent attrbutesActivity = new Intent(this,VerifyActivity.class);
        startActivityForResult(attrbutesActivity, 21);
    }

    private void showTrustedDevices() {
        Intent trustedDevicesActivity = new Intent(this, DeviceSettingsActivity.class);
        startActivity(trustedDevicesActivity);
    }

    // Sign out user
    private void signOut() {
        user.signOut();
        exit();
    }

    // Initialize this activity
    private void init() {
        // Get the user name
        Bundle extras = getIntent().getExtras();
        username = AppHelper.getCurrUser();
        user = AppHelper.getPool().getUser(username);
        getDetails();
    }

    GetDetailsHandler detailsHandler = new GetDetailsHandler() {
        @Override
        public void onSuccess(CognitoUserDetails cognitoUserDetails) {
            closeWaitDialog();
            // Store details in the AppHandler
            AppHelper.setUserDetails(cognitoUserDetails);
            //showAttributes();
            // Trusted devices?
            handleTrustedDevice();
        }

        @Override
        public void onFailure(Exception exception) {
            closeWaitDialog();
            showDialogMessage("Could not fetch user details!", AppHelper.formatException(exception), true);
        }
    };

    private void handleTrustedDevice() {
        CognitoDevice newDevice = AppHelper.getNewDevice();
        if (newDevice != null) {
            AppHelper.newDevice(null);
            trustedDeviceDialog(newDevice);
        }
    }

    private void updateDeviceStatus(CognitoDevice device) {
        device.rememberThisDeviceInBackground(trustedDeviceHandler);
    }

    private void trustedDeviceDialog(final CognitoDevice newDevice) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Remember this device?");
        //final EditText input = new EditText(UserActivity.this);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        //input.setLayoutParams(lp);
        //input.requestFocus();
        //builder.setView(input);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    //String newValue = input.getText().toString();
                    showWaitDialog("Remembering this device...");
                    updateDeviceStatus(newDevice);
                    userDialog.dismiss();
                } catch (Exception e) {
                    // Log failure
                }
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    userDialog.dismiss();
                } catch (Exception e) {
                    // Log failure
                }
            }
        });
        userDialog = builder.create();
        userDialog.show();
    }

    // Callback handlers

    UpdateAttributesHandler updateHandler = new UpdateAttributesHandler() {
        @Override
        public void onSuccess(List<CognitoUserCodeDeliveryDetails> attributesVerificationList) {
            // Update successful
            if(attributesVerificationList.size() > 0) {
                showDialogMessage("Updated", "The updated attributes has to be verified",  false);
            }
            getDetails();
        }

        @Override
        public void onFailure(Exception exception) {
            // Update failed
            closeWaitDialog();
            showDialogMessage("Update failed", AppHelper.formatException(exception), false);
        }
    };

    GenericHandler deleteHandler = new GenericHandler() {
        @Override
        public void onSuccess() {
            closeWaitDialog();
            // Attribute was deleted
            Toast.makeText(getApplicationContext(), "Deleted", Toast.LENGTH_SHORT);

            // Fetch user details from the the service
            getDetails();
        }

        @Override
        public void onFailure(Exception e) {
            closeWaitDialog();
            // Attribute delete failed
            showDialogMessage("Delete failed", AppHelper.formatException(e), false);

            // Fetch user details from the service
            getDetails();
        }
    };

    GenericHandler trustedDeviceHandler = new GenericHandler() {
        @Override
        public void onSuccess() {
            // Close wait dialog
            closeWaitDialog();
        }

        @Override
        public void onFailure(Exception exception) {
            closeWaitDialog();
            showDialogMessage("Failed to update device status", AppHelper.formatException(exception), true);
        }
    };

    private void showUserDetail(final String attributeType, final String attributeValue) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(attributeType);
        final EditText input = new EditText(MainActivity.this);
        input.setText(attributeValue);

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        input.requestFocus();
        builder.setView(input);

        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    String newValue = input.getText().toString();
                    if(!newValue.equals(attributeValue)) {
                        showWaitDialog("Updating...");
                        updateAttribute(AppHelper.getSignUpFieldsC2O().get(attributeType), newValue);
                    }
                    userDialog.dismiss();
                } catch (Exception e) {
                    // Log failure
                }
            }
        }).setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    userDialog.dismiss();
                    deleteAttribute(AppHelper.getSignUpFieldsC2O().get(attributeType));
                } catch (Exception e) {
                    // Log failure
                }
            }
        });
        userDialog = builder.create();
        userDialog.show();
    }

    private void showWaitDialog(String message) {
        closeWaitDialog();
        waitDialog = new ProgressDialog(this);
        waitDialog.setTitle(message);
        waitDialog.show();
    }

    private void showDialogMessage(String title, String body, final boolean exit) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(body).setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    userDialog.dismiss();
                    if(exit) {
                        exit();
                    }
                } catch (Exception e) {
                    // Log failure
                    Log.e(TAG,"Dialog dismiss failed");
                    if(exit) {
                        exit();
                    }
                }
            }
        });
        userDialog = builder.create();
        userDialog.show();
    }

    private void closeWaitDialog() {
        try {
            waitDialog.dismiss();
        }
        catch (Exception e) {
            //
        }
    }

    private void exit () {
        Intent intent = new Intent(getApplicationContext(),
                com.project.falberto.sdcc1617.View.Cognito.MainActivity.class);
        if(username == null)
            username = "";
        intent.putExtra("name",username);
        setResult(RESULT_OK, intent);
        //finish();
        startActivity(intent);
    }
}