package com.project.falberto.sdcc1617.MapeAdaptiveCycle;

/**
 * Created by falberto on 08/03/17.
 */

public enum State {

    POWERSAVING, FULLPOWER, LOWPOWER

}
