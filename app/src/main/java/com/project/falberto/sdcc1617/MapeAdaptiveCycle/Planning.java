package com.project.falberto.sdcc1617.MapeAdaptiveCycle;


import android.util.Log;

/**
 * Bloco di planning del ciclo MAPE
 */
public class Planning {


    public static Plan plan(State newState, State oldState) {

        Plan plan;
        if (newState == State.LOWPOWER && oldState == State.POWERSAVING) {
            plan = Plan.POWERSAVING2LOW;
            Log.i("PLANNING", "Plan.POWERSAVING2LOW");
        } else if (newState == State.POWERSAVING && oldState == State.FULLPOWER) {
            plan = Plan.FULL2POWESAVING;
            Log.i("PLANNING", "Plan.FULL2POWESAVING");
        } else if (newState == State.FULLPOWER && oldState == State.POWERSAVING) {
            plan = Plan.POWERSAVING2FULL;
            Log.i("PLANNING", "Plan.POWERSAVING2FULL");
        } else if (newState == State.POWERSAVING && oldState == State.LOWPOWER) {
            plan = Plan.LOW2POWERSAVING;
            Log.i("PLANNING", "Plan.LOW2POWERSAVING");
        }
        else {
            plan = Plan.DEFAULT;
            Log.i("PLANNING", "Plan.DEFAULT");
        }
        return plan;
    }

}
