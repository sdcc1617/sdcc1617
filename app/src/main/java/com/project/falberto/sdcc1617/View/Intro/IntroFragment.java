package com.project.falberto.sdcc1617.View.Intro;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.project.falberto.sdcc1617.R;


/**
 * Created by falberto on 24/04/16.
 */

public class IntroFragment extends Fragment {

    private static final String BACKGROUND_COLOR = "backgroundColor";
    private static final String PAGE = "page";
    private final static String MY_PREFERENCES = "MyPreferences";
    private static String Username ="Username";
    private int mBackgroundColor, mPage;
    private String stringAnswer;
    public static IntroFragment newInstance(int backgroundColor, int page) {
        IntroFragment frag = new IntroFragment();
        Bundle b = new Bundle();
        b.putInt(BACKGROUND_COLOR, backgroundColor);
        b.putInt(PAGE, page);
        frag.setArguments(b);
        return frag;
    }
    private EditText userName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!getArguments().containsKey(BACKGROUND_COLOR))
            throw new RuntimeException("Fragment must contain a \"" + BACKGROUND_COLOR + "\" argument!");
        mBackgroundColor = getArguments().getInt(BACKGROUND_COLOR);

        if (!getArguments().containsKey(PAGE))
            throw new RuntimeException("Fragment must contain a \"" + PAGE + "\" argument!");
        mPage = getArguments().getInt(PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Select a layout based on the current page
        int layoutResId;
        switch (mPage) {
            case 0:
                layoutResId = R.layout.intro_fragment_layout_1;
                break;
            default:
                layoutResId = R.layout.intro_fragment_layout_2;
        }


        // Inflate the layout resource file
        View view = getActivity().getLayoutInflater().inflate(layoutResId, container, false);

        if( layoutResId == R.layout.intro_fragment_layout_1){

        }
        else if( layoutResId == R.layout.intro_fragment_layout_2){
            /*userName = (EditText) view.findViewById(R.id.editTextfrag2);

            Button b = (Button) view.findViewById(R.id.go);

            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    stringAnswer = userName.getText().toString();
                    SharedPreferences pref = getActivity().getApplicationContext().
                            getSharedPreferences(MY_PREFERENCES, MainActivity.MODE_PRIVATE);
                    SharedPreferences.Editor ed = pref.edit();
                    ed.putString(Username, stringAnswer).commit();
                    getActivity().finish();
                }
            });*/
        }
        // Set the current page index as the View's tag (useful in the PageTransformer)
        view.setTag(mPage);

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Set the background color of the root view to the color specified in newInstance()
        View background = view.findViewById(R.id.intro_background);
        background.setBackgroundColor(mBackgroundColor);
    }

    public int getmBackgroundColor(){
        return mBackgroundColor;
    }
}