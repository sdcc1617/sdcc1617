package com.project.falberto.sdcc1617.View.Intro;


import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by falberto on 24/04/16.
 */
public class IntroAdapter extends FragmentPagerAdapter {
    public IntroAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return IntroFragment.newInstance(Color.parseColor("#FFFFFF"), position); // viola
            default:
                return IntroFragment.newInstance(Color.parseColor("#FFFFFF"), position); // indigo
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

}